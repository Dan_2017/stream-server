// Remote XHR server

const express    = require('express');
const path       = require('path');
const messenger  = require('messenger');
const bodyParser = require('body-parser');
const base64Img  = require('base64-img');
const psTree     = require('ps-tree');

var t = function() {
  var d  = new Date();
  return d;
}

var t2 = function() {
  var d  = new Date();
  var ds = d.toUTCString();
  return ds;
}

var kill = function (pid, signal, callback) {
  signal   = signal || 'SIGKILL';
  callback = callback || function () {};
  var killTree = true;
  if(killTree) {
    psTree(pid, function (err, children) {
      [pid].concat(
        children.map(function (p) {
          return p.PID;
        })
      ).forEach(function (tpid) {
        try { process.kill(tpid, signal) }
        catch (ex) { }
      });
      callback();
    });
  } else {
    try { process.kill(pid, signal) }
    catch (ex) { }
    callback();
  }
};


var app = express();
// var expressWs = require('express-ws')(app);

var msgSpeaker  = messenger.createSpeaker(8000);
var msgListener = messenger.createListener(8000);

// msgListener2.on('maintainance info', function(message, data){
//   message.reply({'res':'Ok'});
//   // process.stdout.write(data);
//   // console.log(data);
// });

app.use('/static', express.static('../client'));

app.use(bodyParser());

app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname + '/../client/html/remote.xhr/test.html'));
});

app.post('/framestream', function(req, res) {
  console.log(req.body.timestamp);
  console.log(req.body.imagedata.length);

  var filepath = base64Img.imgSync('data:image/jpeg;base64,' + req.body.imagedata, '../client/img/', 'test_picture_stream');
  
  console.log(filepath);

  res.send("Ok");
});

var servoState  = 0x00;
var streamState = "stop";

app.get('/servostate', function(req, res) {
  if (servoState) res.send({"val":servoState});
  else if(streamState) res.send({"val":-1, "stream":streamState});
  servoState  = -1;
  streamState = "continue";
});

app.post('/servostate', function(req, res){
  servoState  = req.body.val;
  streamState = req.body.stream;
  res.send({"val":servoState, "stream":streamState, "body":req.body, "params": req.params});
});

var Channel = {
  "peer-1" : {msgs:[]},
  "peer-2" : {msgs:[]}
};

app.get('/chamnnel1', function (req, res) {
  res.send({type:"data channel 1"});
});

app.post('/channel1', function (req, res) {
  console.log(t2() + ' [MSG]', req.body);
  if (req.body.peer) {
    var m = req.body;

    if(!m.msgs) m.msgs = [];
    if (m.peer == 1) {
      for(var i = 0; i < m.msgs.length; i++) {
        var m2 = m.msgs[i];
        Channel['peer-1'].msgs.push ({
          msg     : m2.msg, 
          payload : (m2.payload) ? m2.payload : ''
        });
      }

      res.send(Channel['peer-2']);

      Channel['peer-2'].msgs = [];

      if(m.msgs.length) { 
        console.log('[MSGS]', m.msgs); 

        console.log(Channel['peer-1']);

        console.log('Message from peer 2 redirected to peer 1');
        // console.log('Channel.peerMsg1: ', Channel['peer-1']);
        // console.log('Channel.peerMsg2: ', Channel['peer-2']);
      }
    } 
    if (m.peer == 2) {
      for(var i = 0; i < m.msgs.length; i++) {
        var m2 = m.msgs[i];
        Channel['peer-2'].msgs.push ({
          msg     : m2.msg, 
          payload : (m2.payload) ? m2.payload : ''
        });
      }

      res.send(Channel['peer-1']);
      
      Channel['peer-1'].msgs = [];

      if(m.msgs.length) {
        console.log('[MSGS]', m.msgs);
        
        console.log(Channel['peer-2']);

        console.log('Message from peer 1 redirected to peer 2');
        // console.log('Channel[peer-1]: ', Channel['peer-1']);
        // console.log('Channel[peer-2]: ', Channel['peer-2']);
      }
    }
  } else {
    res.send({res:"ok"});
  }
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});