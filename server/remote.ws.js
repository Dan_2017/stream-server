// Remote websocket server

const base64Img  = require('base64-img');
const path       = require('path');
const messenger  = require('messenger');
const bodyParser = require('body-parser');
const express    = require('express');
const app        = express();
const server     = require('http').createServer(app);
const io         = require('socket.io')(server);

var t = function() {
  var d  = new Date();
  return d;
}

var t2 = function() {
  var d  = new Date();
  var ds = d.toUTCString();
  return ds;
}

app.use('/static', express.static('../client'));

app.use(bodyParser());

app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname + '/../client/html/remote.ws/test.html'));
});


app.get('/ping', function(req, res) {
	res.send("OK");
});


var servoState  = 0x00;
var streamState = "stop";

app.get('/servostate', function(req, res) {
  if (servoState) res.send({"val":servoState});
  else if(streamState) res.send({"val":-1, "stream":streamState});
  servoState  = -1;
  streamState = "continue";
});

app.post('/servostate', function(req, res){
  servoState  = req.body.val;
  streamState = req.body.stream;
  res.send({"val":servoState, "stream":streamState, "body":req.body, "params": req.params});
});



io.on('connection', function(socket){ 
	console.log('[INCOMING CONNECTION] ', socket.id); 
	socket.on('test', function(data){
		console.log('[TEST] ', data);
		console.log('[SOCKET]', socket.id);
	});
	socket.on('client', function(data){
		console.log('[CLIENT] ', data);
		console.log('[SOCKET]', socket.id);
		socket.join('viewers');
	});
	socket.on('image', function(data) {
		console.log('[SOCKET 2 IMG] ', data.length);
		var filepath = base64Img.imgSync('data:image/jpeg;base64,' + data, '', 'test_picture_stream');
		io.to('viewers').emit('frame', data);
	});
});

server.listen(3000, function(){
	console.log('HTTP Server started at :3000');
	console.log('WebSocket Server started at :3000');
});