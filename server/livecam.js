const LiveCam    = require('livecam');
const fs         = require('fs');

var webcam_settings = JSON.parse(fs.readFileSync("webcam.config.json", "utf-8"));
console.dir(webcam_settings);
(webcam_settings.grayscale == 'true') ?
  webcam_settings.grayscale = true :
  webcam_settings.grayscale = false ;

(webcam_settings.fake == 'true') ?
  webcam_settings.fake = true :
  webcam_settings.fake = false ;

const webcam_server = new LiveCam({
    // address and port of the webcam UI
    'ui_addr' : '0.0.0.0',
    'ui_port' : 11000,

    // address and port of the webcam Socket.IO server
    // this server broadcasts GStreamer's video frames
    // for consumption in browser side.
    'broadcast_addr' : '192.168.1.221',
    'broadcast_port' : 12000,

    // address and port of GStreamer's tcp sink
    'gst_tcp_addr' : '0.0.0.0',
    'gst_tcp_port' : 10000,

    'start' : function() {
        console.log('WebCam server started!');
    },
    
    // webcam object holds configuration of webcam frames
    'webcam' : webcam_settings
});

webcam_server.broadcast();
