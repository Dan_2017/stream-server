//local xhr server

const fs         = require('fs');
const path       = require('path');
const nodeWebcam = require('node-webcam');
const base64Img  = require('base64-img');
const express    = require('express');
const watch      = require('node-watch');
const ajax       = require('ajax-request');
const messenger  = require('messenger');
const bodyParser = require('body-parser');
const i2c        = require('i2c');
const speedTest  = require('speedtest-net');
const io         = require('socket.io-client');
const psTree     = require('ps-tree');

var t = function() {
  var d  = new Date();
  return d;
}

var t2 = function() {
  var d  = new Date();
  var ds = d.toUTCString();
  return ds;
}

var log = function(m){
	fs.appendFileSync('/home/fa/dev/cam_xhr/cam.log', t() + '\n');
	for (var i = 0; i < m.length; i++) {
		console.log(m[i]);
		if (typeof m[i] == 'object'){
			fs.appendFileSync('/home/fa/dev/cam_xhr/cam.log', JSON.stringify(m[i]) + '\n');
		} else {
			fs.appendFileSync('/home/fa/dev/cam_xhr/cam.log', m[i] + '\n');
		}
	}
	fs.appendFileSync('/home/fa/dev/cam_xhr/cam.log', '\n\n');
}

var kill = function (pid, signal, callback) {
  signal   = signal || 'SIGKILL';
  callback = callback || function () {};
  var killTree = true;
  if(killTree) {
    psTree(pid, function (err, children) {
      [pid].concat(
        children.map(function (p) {
          return p.PID;
        })
      ).forEach(function (tpid) {
        try { process.kill(tpid, signal) }
        catch (ex) { }
      });
      callback();
    });
  } else {
    try { process.kill(pid, signal) }
    catch (ex) { }
    callback();
  }
};

var test = speedTest({maxTime: 1000});
console.log('[TEST INET SPEED]');

test.on('data', function(data) {
	console.dir(data);

	app.listen(8080, function() {
		log(['Listening on 8080']);
		ServoState.open();
	});

	var socket1 = io('http://192.168.1.221:12000');
	var socket2 = io('http://192.168.1.221:12000');

	socket1.on('connect', function(){
		console.log('[SOCKET 1 ID]', socket1.id);
	});

	socket2.on('connect', function(){
		console.log('[SOCKET 2 ID]', socket2.id);
	});

	console.log('[SOCKET 1 ID]', socket1.id);
	console.log('[SOCKET 2 ID]', socket2.id);

});

var DEVICE_ADDR = 0x08;
// var MSG_BYTE    = 0x01;

var wire = new i2c(DEVICE_ADDR, {device: '/dev/i2c-0'});

// var msgSpeaker = messenger.createSpeaker(8000);
//const ws = new WebSocket('ws://62.109.21.228:3000/echo');

// log file
fs.writeFileSync('/home/fa/dev/cam_xhr/cam.log', '');

const app = express();

app.use(bodyParser.json());

app.get("/", function(req,res){
	res.send({server:"node webcam", version:"0.0.1"});
});


app.get("/test", function(req, res){
	var d = new Date(), ts = d.getTime();
	res.send({msg:"test", timestamp:ts});
});

var opts = {
    width   : 320,
    height  : 240,
    delay   : 0,
    quality : 80,
    output  : "jpeg",
    verbose : true
};
 
var webcam = nodeWebcam.create(opts);

var snapshot = function() {
	// console.log('[Taking snapshot]');
	log(['[Taking snapshot]']);

	webcam.capture( "/home/fa/dev/cam_xhr/assets/img/test_picture.jpg", function(err, data) {
		var d = new Date(), ds = d.toUTCString();
		// console.log('[IMG UP]', '[' + ds + ']');
		log(['[IMG UP]', '[' + ds + ']']);

		// msgSpeaker.request (
		// 	'debug info', 
		// 	{'msg':'image captured at '+ds}, 
		// 	function(message, data) {}
		// );
		sendSnapshot();
	});
};

var sendSnapshot = function(){
	var imageData = base64_encode('/home/fa/dev/cam_xhr/assets/img/test_picture.jpg');
	Channel.sendMessage('snapshot', imageData);
}

var base64_encode = function(file) {
	var bitmap = fs.readFileSync(file);
	return new Buffer(bitmap).toString("base64");
} 

var servoposition = function(val){
	log(['[SERVOPOSITION]', val]);

	wire.write([val], function(err){
		if(err) throw err;
		Channel.sendMessage('servoposition', val);
	});
}

// watch('./assets/img/test_picture.jpg', {recursive: true}, function(){
// 	var d = new Date(), ds = d.toUTCString();
// 	console.log('[IMG UP]', '[' + ds + ']');

//   sendSnapshot();
// });
	var FRAMESTREAM_LAT = 1000;

	FrameStream = {
		isActive   : false,
		xhr        : null,
		xhrTimeout : null,
		open       : function() {
			FrameStream.isActive = true;
			console.log('[FRAMESTREAM] ', 'Open');
			var req = function() {
				// FrameStream.xhr = ajax.post({
				ajax.post({
  						url  : "http://54.244.199.216:3000/framestream",
  						// url  : "http://62.109.21.228:3000/framestream",
  						// url  : "http://192.168.1.59:3000/framestream",
						data : FrameStream.frameData
					},
					function( err, res, body ) {
						if(err) {
							// console.log('[XHR ERROR] ', err);
							log(['[XHR ERROR] ', err]);

							if (FrameStream.isActive) FrameStream.xhrTimeout = setTimeout(req, FRAMESTREAM_LAT);
							return;	
						} else {

		    				log(['[Taking snapshot]']);

							webcam.capture( 
								"/home/fa/dev/cam_xhr/assets/img/test_picture.jpg", 
								function(err, data) {
									log(['[IMG UP]', '[' + t() + ']']);
				    				
				    				FrameStream.frameData = {
				    					"timestamp": t(),
				    					"imagedata": base64_encode('/home/fa/dev/cam_xhr/assets/img/test_picture.jpg')
				    				};
				    				console.log('[FRAMESTREAM]', FrameStream.frameData.timestamp, FrameStream.frameData.imagedata.length);
				    				if (FrameStream.isActive) FrameStream.xhrTimeout = setTimeout(req, FRAMESTREAM_LAT);
								}
							);
						}
					}
				);
			}
			req();
		},
		close      : function() {
			FrameStream.isActive = false;
			clearTimeout(FrameStream.xhrTimeout);
		},
		frameData  : ""
	};


	var SERVOSTATE_LAT = 250;

	ServoState = {
		isActive   : false,
		xhr        : null,
		xhrTimeout : null,
		open       : function () {
			console.log('[SERVOSTATE] ', 'Open');
			ServoState.isActive = true;
			var req = function() {
				// FrameStream.xhr = ajax.post({
				ajax(
  						"http://54.244.199.216:3000/servostate",
  						// "http://62.109.21.228:3000/servostate",
  						// "http://192.168.1.59:3000/servostate",
					function( err, res, body ) {
						if(err) {
							// console.log('[XHR ERROR] ', err);
							log(['[XHR ERROR] ', err]);

							if (ServoState.isActive) ServoState.xhrTimeout = setTimeout(req, SERVOSTATE_LAT);
							return;	
						} else {
	    					console.log('[BODY]', body);
	    					var pos = JSON.parse(body);
	    					if (pos.stream) {
	    						if(pos.stream == 'start'){
	    							FrameStream.open();
	    						} else if(pos.stream == 'stop'){
	    							FrameStream.close();
	    						}
	    					} else if (pos.val >= 0) {
		    					log(['[Setting servo position] ', pos.val]);
								wire.write([pos.val * 1], function(err){
									if(err) console.log(err);
								});
	    					}

							if (ServoState.isActive) ServoState.xhrTimeout = setTimeout(req, SERVOSTATE_LAT);
						}
					}
				);
			}
			req();
		},
		close      : function () {
			ServoState.isActive = false;
			clearTimeout(ServoState.xhrTimeout);
		},
		servoState : null
	}

	var CHANNEL_UPDATE = 250;

	// Channel = {
	// 	open: function() {
	// 		var xhr = function() {
	// 			// console.log('[XHR]');
	// 			ajax.post({
	//   					url  : "http://62.109.21.228:3000/channel1",
	//   					// url  : "http://192.168.1.59:3000/channel1",
	//   					data : Channel.messages
	// 				},
	// 				function( err, res, body ) {
	// 					if(err) {
	// 						// console.log('[XHR ERROR] ', err);
	// 						log(['[XHR ERROR] ', err]);

	// 						Channel.poolingId = setTimeout(xhr, CHANNEL_UPDATE);
	// 						return;	
	// 					} else {
	// 						Channel.messages.msgs = [];
	// 	    				var msg = JSON.parse(body);
	// 	    				if(!msg.msgs) msg.msgs = [];
	// 		    			for(var i = 0; i < msg.msgs.length; i++){
	// 							Channel.onMessage(msg.msgs[i]);	
	// 		    			}
	// 	    				Channel.poolingId = setTimeout(xhr, CHANNEL_UPDATE);
	// 					}
	// 				}
	// 			);
	// 		}
	// 		xhr();
	// 	},
	// 	sendMessage: function(msg, payload) {
	// 		Channel.messages.msgs.push({
	// 			msg     : msg,
	// 			payload : payload
	// 		});
	// 		// console.log('[MESSAGES LENGTH]', Channel.messages.length);
	// 		log(['[MESSAGES LENGTH]', Channel.messages.msgs.length]);
	// 	},
	// 	onMessage: function(msg) {
	// 		if(!msg.msg) return; 
	// 		var m = msg.msg;
	// 		// console.log('[INCOMING MESSAGE] ', msg.msg);
	// 		log(['[INCOMING MESSAGE] ', msg.msg]);

	// 		if (Channel.msgHandlers[m]) Channel.msgHandlers[m](msg);
	// 	},
	// 	close: function() {
	// 		clearTimeout(Channel.poolingId);
	// 	},
	// 	off: function(msg) {
	// 		delete Channel.msgHandlers[msg];
	// 	},
	// 	on: function(msg, handler) {
	// 		Channel.msgHandlers[msg] = handler;
	// 	},
	// 	msgHandlers : {},
	// 	messages    : {peer:2, msgs: [{msg: "67890", payload: ''}]},
	// 	poolingId   : null
	// }

// app.listen(8080, function () {
  
//   // console.log('Listening on 8080');
//   log(['Listening on 8080']);

//   // Channel.on("hi", function(msg) {
//   // 	// console.log('[HI MESSAGE HANDLER] ', msg);
//   // 	log(['[HI MESSAGE HANDLER] ', msg]);
//   // });
  
//   // Channel.on("snapshot", function(msg) {
//   // 	// console.log('[SNAPSHOT MESSAGE HANDLER] ', msg);
//   // 	log(['[SNAPSHOT MESSAGE HANDLER] ', msg]);
  	
//   // 	snapshot();
//   // });
  
//   // Channel.on("servoposition", function(msg) {
//   // 	// console.log('[SNAPSHOT MESSAGE HANDLER] ', msg);
//   // 	log(['[SERVOPOSITION MESSAGE HANDLER] ', msg]);
  	
//   // 	servoposition(msg.payload * 0x01);
//   // });
  
//   // Channel.open();

//   // FrameStream.open();

//   ServoState.open();
// });