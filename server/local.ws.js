// local websocket server

const express    = require('express');
const fs         = require('fs');
const path       = require('path');
const messenger  = require('messenger');
const bodyParser = require('body-parser');
const base64Img  = require('base64-img');
const i2c        = require('i2c');
const exec       = require('child_process').exec;
const psTree     = require('ps-tree');
const ioc        = require('socket.io-client');
const ajax       = require('ajax-request');
const speedTest  = require('speedtest-net');


var t = function() {
  var d  = new Date();
  return d;
}

var t2 = function() {
  var d  = new Date();
  var ds = d.toUTCString();
  return ds;
}

var log = function(m){
  fs.appendFileSync('/home/fa/dev/stream-server/logs/cam.log', t() + '\n');
  for (var i = 0; i < m.length; i++) {
    console.log(m[i]);
    if (typeof m[i] == 'object'){
      fs.appendFileSync('/home/fa/dev/stream-server/logs/cam.log', JSON.stringify(m[i]) + '\n');
    } else {
      fs.appendFileSync('/home/fa/dev/stream-server/logs/cam.log', m[i] + '\n');
    }
  }
  fs.appendFileSync('/home/fa/dev/stream-server/logs/cam.log', '\n\n');
}

var kill = function (pid, signal, callback) {
  signal   = signal || 'SIGKILL';
  callback = callback || function () {};
  var killTree = true;
  if(killTree) {
    psTree(pid, function (err, children) {
      [pid].concat(
        children.map(function (p) {
          return p.PID;
        })
      ).forEach(function (tpid) {
        try { process.kill(tpid, signal) }
        catch (ex) { }
      });
      callback();
    });
  } else {
    try { process.kill(pid, signal) }
    catch (ex) { }
    callback();
  }
};

var ping = function() {
  ajax({
    url  : "http://54.244.199.216:3000/ping",
    data : {ping:"pong"}
    },
    function( err, res, body ) {
      if(err) {
        log(['[XHR ERROR] ', err]);
        ping();
        return; 
      }
      log(['[APP SERVER AVAILABLE] ', body]);
      start();
    }
  );
};

var start = function() {
  socket1 = ioc('http://54.244.199.216:3000/');
  socket2 = ioc('http://192.168.1.221:12000');

  socket1.on('connect', function() {
    console.log('[SOCKET 1 ID] ', socket1.id);
  });

  socket2.on('connect', function() {
    console.log('[SOCKET 2 ID] ', socket2.id);
  });

  socket2.on('image', function(data) {
    // console.log(frameCount);
    frameCount++;
    if(frameCount%10 == 0) {
      console.log('[SOCKET 2 IMG] ', frameCount/10, data.length);
      socket1.emit("image", data);
    }
  });

  var test = speedTest({maxTime: 1000});
  console.log('[TEST INET SPEED]');

  test.on('data', function(data) {
    console.dir(data);

    app.listen(3000, function () {
      console.log('Example app listening on port 3000!');
      ServoState.open();
    });

  });

};


var socket1, socket2;

var frameCount = 0;

var DEVICE_ADDR = 0x08;

var wire = new i2c(DEVICE_ADDR, {device: '/dev/i2c-0'});

var app = express();

app.use('/static', express.static('../client'))

app.use(bodyParser());

app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname + '/../client/html/local.ws/lancam.html'));
});

app.post('/camstate', function (req, res) {
    var camturn = req.body.val;
    wire.write([camturn], function(err) {
        if(err) throw err;
    });
    res.send("...OK");
});

app.post('/cam', function (req, res) {
    if(req.body.settings) {
      setupWebcam();
    }

    if (livecam) {
      kill(livecam.id);
      livecam = null;
    }

    if(req.body.state == "on") {
      startWebcam();
      res.send("cam ON");
    } else {
      res.send("cam OFF");
    }
});


var livecam = null;

var webcam_settings = {
  grayscale : false,
  width     : 240,
  height    : 180,
  fake      : false,
  framerate : 15
};

var setupWebcam = function() {
  fs.writeFileSync("webcam.config.json", JSON.stringify(webcam_settings));
}

var startWebcam = function(cb){
  livecam = exec('node ./livecam.js', cb);
  console.log('[LIVECAM] START');

  livecam.stdout.on('data', function(data){
    console.log('[LIVECAM] ', data);
  });
};

var stopWebcam = function(cb) {
  console.log('[LIVECAM] STOP');
  if(livecam) {
    console.log('[LIVECAM PID] ', livecam.pid);      
    kill(livecam.pid, 'SIGTERM', function() {
      livecam = null;
    });
  }
};

var SERVOSTATE_LAT = 250;

ServoState = {
  isActive   : false,
  xhr        : null,
  xhrTimeout : null,
  open       : function () {
    console.log('[SERVOSTATE] ', 'Open');
    ServoState.isActive = true;
    var req = function() {
      // FrameStream.xhr = ajax.post({
      ajax(
            "http://54.244.199.216:3000/servostate",
            // "http://62.109.21.228:3000/servostate",
            // "http://192.168.1.59:3000/servostate",
        function( err, res, body ) {
          if(err) {
            // console.log('[XHR ERROR] ', err);
            log(['[XHR ERROR] ', err]);

            if (ServoState.isActive) ServoState.xhrTimeout = setTimeout(req, SERVOSTATE_LAT);
            return; 
          } else {
              console.log('[BODY]', body);
              var pos = JSON.parse(body);
              if (pos.stream) {
                if(pos.stream == 'start'){
                  startWebcam();
                  // FrameStream.open();
                } else if(pos.stream == 'stop'){
                  stopWebcam();
                  // FrameStream.close();
                }
              } else if (pos.val >= 0) {
                log(['[Setting servo position] ', pos.val]);
              wire.write([pos.val * 1], function(err){
                if(err) console.log(err);
              });
              }

            if (ServoState.isActive) ServoState.xhrTimeout = setTimeout(req, SERVOSTATE_LAT);
          }
        }
      );
    }
    req();
  },
  close      : function () {
    ServoState.isActive = false;
    clearTimeout(ServoState.xhrTimeout);
  },
  servoState : null
}


ping();