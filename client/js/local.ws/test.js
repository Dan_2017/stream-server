$(function(){
	var t = function() {
		var d = new Date();
		return d.toUTCString();;
	}
	var ts = function() {
		var d = new Date();
		return d.getTime();
	}
	
	const KEY = 123;

	var accessGranted = false;

	var sectionAuth  = $("#section-auth"),
	    sectionSnap  = $("#section-snap"),
	    sectionAbout = $("#section-about"),
	    sectionHelp  = $("#section-help");

	//sectionSnap.hide();
	//sectionAbout.hide();
	//sectionHelp.hide();
	//$("#snap-progress").hide();

	$("#snap-title").html("Snapshot [" + t() + "]");

	$("#enter-button").on("click", function(){
		// console.log($("#access-key").val());
		if($("#access-key").val() == KEY){
			accessGranted = true;
			sectionAuth.hide();
			sectionSnap.show();
			//WebSocketTest();

			// console.log('[POOLING XHR CHANNEL]');
			// Channel.on("12345", function(m){
			// 	console.log('[12345 message handler]', m);
			// });
			// Channel.on("snapshot", function(m) {
			// 	console.log('[Snapshot message handler]', m);
			// 	$("#snapshot").attr("src", "data:image/png;base64," + m.payload);
			// 	$("#snap-progress").hide();
			// 	$('#get-snapshot-button').removeAttr('disabled');
			// 	$('#servo-switch-1').removeAttr('disabled');
			// 	$("#snap-title").html("Snapshot [" + t() + "]");
			// });
			// Channel.on("servoposition", function() {
			// 	$('#get-snapshot-button').removeAttr('disabled');
			// 	$('#servo-switch-1').removeAttr('disabled');
			// });
			// Channel.open();

		} else {
			$("#access-key-field").addClass("is-invalid");
			return;
		}
	});
	$("#access-key").on("change", function() {
		$("#access-key-field").removeClass("is-invalid");
	});

	// $("#get-snapshot-button").on("click", function() {
	// 	if($(this).attr('disabled')) return;
	// 	console.log('[Taking snapshot]');
	// 	$("#snap-progress").show();
	// 	$('#get-snapshot-button').attr('disabled', true);
	// 	$('#servo-switch-1').attr('disabled', true);
	// 	Channel.sendMessage('snapshot');
	// 	// ws.send('cam 1 {"cmd":"getSnapshot"}');
	// 	//setTimeout(function(){
	// 	//	$("#snap-progress").hide();
	// 	//	$("#snap-title").html("Snapshot [" + t() + "]");
	// 	//}, 3000);
	// });

	$("#stream-control").on("click", function() {
		console.log($(this).text());
		// FrameStream.start();
		$.post({url:"/cam", data:{state:"on"}})
			.done(function(responce) {
				console.log('[CAM] ', responce);
				setTimeout(function(){
				$("#stream-frame")
					.attr(
						"src", 
						"http://192.168.1.221:11000?"+ts()
					);
				}, 5000);
			});
	});

	$("#servo-switch-1").on("click", function() {
		var val = 0;
		if($(this).prop('checked')) {
			val = 1;
			// Channel.sendMessage('servoposition', '0x01');
		} else {
			val = 0;
			// Channel.sendMessage('servoposition', '0x00');
		}
		$.post({url:"/servostate", data:{val:val}})
			.done(function(responce) {
				console.log('[SERVO] ', responce);
			});

		// $('#get-snapshot-button').attr('disabled', true);
		// $('#servo-switch-1').attr('disabled', true);
	});
	
	$("#turn-cam-left").on("click", function() {
		$.post({url:"/camstate", data:{val:0x01}})
			.done(function(responce) {
				console.log('[CAM] turn left ', responce);
			});
	});

	$("#turn-cam-down").on("click", function() {
		$.post({url:"/camstate", data:{val:0x07}})
			.done(function(responce) {
				console.log('[CAM] turn left ', responce);
			});
	});
	
	$("#turn-cam-right").on("click", function() {
		$.post({url:"/camstate", data:{val:0x00}})
			.done(function(responce) {
				console.log('[CAM] turn right ', responce);
			});
	});

	$("#turn-cam-up").on("click", function() {
		$.post({url:"/camstate", data:{val:0x08}})
			.done(function(responce) {
				console.log('[CAM] turn right ', responce);
			});
	});
	
	$("a.mdl-navigation__link").on("click", function(e){
		e.preventDefault();
		if(!accessGranted){
			console.log("Permission denied");
			sectionAuth.show();
			sectionSnap.hide();
			sectionAbout.hide();
			sectionHelp.hide();

			return;
		}
		switch ($(this).html()) {
			case "Camera":
				console.log("Camera");
				sectionSnap.show();
				sectionAbout.hide();
				sectionHelp.hide();
			break;
			
			case "About":
				console.log("About");
				sectionSnap.hide();
				sectionAbout.show();
				sectionHelp.hide();
			break;

			case "Help":
				console.log("Help");
				sectionSnap.hide();
				sectionAbout.hide();
				sectionHelp.show();
			break;
 
		}
		$(".mdl-layout__drawer").removeClass("is-visible");
		$(".mdl-layout__obfuscator").removeClass("is-visible");

		return false;
	});

	// var CHANNEL_UPDATE  = 250,
	// 	CHANNEL_TIMEOUT = 10000;

	// Channel = {
	// 	xhr   : null,  // XHR object
	// 	watch : null,  // XHR watch timeout ID
	// 	open  : function() {
	// 		var xhr = function() {
	// 			Channel.xhr = $.post({
	//   				url  : "/channel1",
	//   				data : Channel.messages
	// 			})
	//   			.done(function( msg ) {
	//     			clearTimeout(Channel.watch);
	//     			Channel.messages.msgs = [];
	//     			for(var i = 0; i < msg.msgs.length; i++){
	// 					Channel.onMessage(msg.msgs[i]);	    				
	//     			}
	//     			Channel.poolingId = setTimeout(xhr, CHANNEL_UPDATE);
	// 			});

	// 		}
	// 		xhr();
	// 		Channel.watch = setTimeout(function(){
	// 			console.warn("[REQUEST TIMEOUT]");
	// 			Channel.xhr.abort();
	// 		}, CHANNEL_TIMEOUT);
	// 	},
	// 	sendMessage: function(msg, payload) {
	// 		Channel.messages.msgs.push({
	// 			msg     : msg,
	// 			payload : payload
	// 		});
	// 	},
	// 	onMessage: function(msg) {
	// 		if(!msg.msg) return;
	// 		var m = msg.msg;
	// 		console.info('[INCOMING MESSAGE] ', msg.msg);
	// 		if (Channel.msgHandlers[m]) Channel.msgHandlers[m](msg);
	// 	},
	// 	on: function(msg, handler) {
	// 		Channel.msgHandlers[msg] = handler;
	// 	},
	// 	off: function(msg) {
	// 		delete Channel.msgHandlers[msg];
	// 	},
	// 	close: function() {
	// 		clearTimeout(Channel.poolingId);
	// 	},
	// 	msgHandlers : {},
	// 	messages    : {peer:"1", msgs:[{msg:"hi", payload:"12345"}]},
	// 	poolingId   : null
	// }

	FrameStream = {
		timeoutId: null,
		progress: 0,
		start: function() {
			var title    = $("#snap-title"),
				control  = $("#stream-control"),
				snapshot = $("#snapshot"),
				shader   = $("#snap-shader"),
				progress = $("#snap-progress-2");

			control
				.html("Stop stream")
				.off("click")
				.on("click", function(){
					FrameStream.stop();
				});

			snapshot[0]
				.onload = function() {
					// console.log(this);
					progress.css('visibility', 'visible');
					shader.css("opacity", 0);
					FrameStream.progress = 0;
					title.html("Snapshot [" + t() + "]");
					FrameStream.timeoutId = setTimeout(animate, 1000);
				}

			var animate = function() {

				if(FrameStream.progress >= 100) {
					shader.css("opacity", 1);
					progress[0].MaterialProgress.setProgress(100);
					setTimeout(function(){
						progress.css('visibility', 'hidden');
						progress[0].MaterialProgress.setProgress(0);
						snapshot.attr('src', 
							'/static/image/test_picture_stream.jpg?timestamp=' + ts()
						);
					}, 500)
				} else {
					// shader.css("opacity", FrameStream.progress/100);
					progress[0].MaterialProgress.setProgress(FrameStream.progress);
					FrameStream.progress += 5;
					FrameStream.timeoutId = setTimeout(animate, 100);
				}
			}
			animate();
		},
		stop: function(){
			var control  = $("#stream-control"),
				shader   = $("#snap-shader"),
				progress = $("#snap-progress-2")[0];

			control
				.html("Start stream")
				.off('click')
				.on('click', function() {
					FrameStream.start();
				});
			shader
				.css("opacity", 0);
			progress.MaterialProgress
				.setProgress(0);

			clearTimeout(FrameStream.timeoutId);
		}
	}


	// console.log('[NEW BIDIRECTIONAL XHR CHANNEL]');
	// Channel.open();

	/*
	var ws = null;

	function WebSocketTest(){
		if ("WebSocket" in window){
			console.log("WebSocket is supported by your Browser!");

			// Let us open a web socket
			ws = new WebSocket("ws://62.109.21.228:3000/echo");

			ws.onopen = function(){
				// Web Socket is connected, send data using send()
				ws.send("init pad 1");
				console.log("Init message is sent...");
			};

			ws.onmessage = function (evt){
				var received_msg = evt.data;
				console.log("Message is received:");
				console.log(received_msg);
				if(received_msg != "[ECHO]"){
					$("#snapshot").attr("src", "data:image/png;base64," + received_msg);
					$("#snap-progress").hide();
					$("#snap-title").html("Snapshot [" + t() + "]");
				}
			};

			ws.onclose = function(){
				// websocket is closed.
				console.log("Connection is closed.");
			};
		} else {
			// The browser doesn't support WebSocket
			console.log("WebSocket NOT supported by your Browser!");
		}
	}

	//WebSocketTest();
	*/
});
